<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Domain\Model\Member;

/**
 * Class MemberRepositoryInterface
 *
 * @package Orangear\MembershipBundle\Domain\Model\Member
 */
interface MemberRepositoryInterface
{
    /**
     * @param MemberIdentifierInterface $identifier
     *
     * @return null|MemberInterface
     */
    public function findById(MemberIdentifierInterface $identifier) :? MemberInterface;

    /**
     * @param string $email
     *
     * @return null|MemberInterface
     */
    public function findByEmail(string $email) :? MemberInterface;
}
