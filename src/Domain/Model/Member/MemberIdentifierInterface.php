<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Domain\Model\Member;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\Common\Identifier\IdentifierInterface as IdentifierInterface2;

/**
 * Interface MemberIdentifierInterface
 *
 * @package Orangear\MembershipBundle\Domain\Model
 */
interface MemberIdentifierInterface extends IdentifierInterface, IdentifierInterface2 {}
