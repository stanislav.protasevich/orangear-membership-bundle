<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Domain\Model\Member;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Member
 *
 * @package Orangear\MembershipBundle\Domain\Model\Member
 */
abstract class Member implements MemberInterface, UserInterface
{
    /** @var MemberIdentifierInterface */
    protected $id;

    /** @var string */
    protected $email;

    /** @var string */
    protected $password;

    /** @var string */
    protected $salt;

    /** @var DateTime */
    protected $createdAt;

    /** @var DateTime */
    protected $updatedAt;

    /** @var DateTime */
    protected $deletedAt;

    /** @var ArrayCollection */
    protected $projects;

    /**
     * Member constructor
     *
     * @param MemberIdentifierInterface $id
     * @param string $email
     * @param string $password
     * @param string $salt
     * @param DateTime $createdAt
     * @param DateTime $updatedAt
     * @param DateTime $deletedAt
     */
    protected function __construct(
        MemberIdentifierInterface $id = null,
        string $email = null,
        string $password = null,
        string $salt = null,
        DateTime $createdAt = null,
        DateTime $updatedAt = null,
        DateTime $deletedAt = null
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->salt = $salt;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->deletedAt = $deletedAt;
        $this->projects = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function id(): MemberIdentifierInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function email(): string
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function password(): string
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function salt(): string
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function createdAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function updatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function deletedAt(): DateTime
    {
        return $this->deletedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        throw new \Exception('');
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password();
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return $this->salt();
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->email();
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials() {}

    /**
     * @return ArrayCollection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $payload) : MemberInterface
    {
        return new static(
            $payload['id'] ?? null,
            $payload['email'] ?? null,
            $payload['password'] ?? null,
            $payload['salt'] ?? null,
            $payload['created_at'] ?? null,
            $payload['updated_at'] ?? null,
            $payload['deleted_at'] ?? null
        );
    }
}
