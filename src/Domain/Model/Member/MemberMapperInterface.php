<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Domain\Model\Member;

/**
 * Class MemberMapperInterface
 *
 * @package Orangear\MembershipBundle\Domain\Model\Member
 */
interface MemberMapperInterface
{
    /**
     * @param MemberIdentifierInterface $identifier
     *
     * @return null|MemberInterface
     */
    public function fetchById(MemberIdentifierInterface $identifier) :? MemberInterface;

    /**
     * @param string $email
     *
     * @return null|MemberInterface
     */
    public function fetchByEmail(string $email) :? MemberInterface;
}
