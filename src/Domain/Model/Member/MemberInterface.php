<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Domain\Model\Member;

use DateTime;

/**
 * Interface MemberInterface
 *
 * @package Orangear\MembershipBundle\Domain\Model\Member
 */
interface MemberInterface
{
    /**
     * @return MemberIdentifierInterface
     */
    public function id() : MemberIdentifierInterface;

    /**
     * @return string
     */
    public function email() : string;

    /**
     * @return string
     */
    public function password() : string;

    /**
     * @return string
     */
    public function salt() : string;

    /**
     * @return DateTime
     */
    public function createdAt() : DateTime;

    /**
     * @return DateTime
     */
    public function updatedAt() : DateTime;

    /**
     * @return DateTime
     */
    public function deletedAt() : DateTime;

    /**
     * @param array $payload
     *
     * @return mixed
     */
    public static function fromArray(array $payload) : MemberInterface;
}
