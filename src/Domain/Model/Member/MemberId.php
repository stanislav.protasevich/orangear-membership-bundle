<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Domain\Model\Member;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class MemberId
 *
 * @package Orangear\MembershipBundle\Domain\Model\Member
 */
final class MemberId implements MemberIdentifierInterface
{
    /** @var string */
    private $id;

    /**
     * MemberId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * {@inheritdoc}
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public static function fromString(string $id): IdentifierInterface
    {
        return new self($id);
    }

    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString() : string
    {
        return $this->id;
    }
}
