<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Domain\Model;

/**
 * Interface IdentifierInterface
 *
 * @package Orangear\MembershipBundle\Domain\Model
 */
interface IdentifierInterface
{
    /**
     * @return string
     */
    public function id() : string;

    /**
     * @param string $id
     *
     * @return IdentifierInterface
     */
    public static function fromString(string $id) : IdentifierInterface;

    /**
     * @return string
     */
    public function toString() : string;

    /**
     * @return string
     */
    public function __toString() : string;
}
