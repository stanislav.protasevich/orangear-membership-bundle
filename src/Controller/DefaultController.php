<?php

namespace Orangear\MembershipBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OrangearMembershipBundle:Default:index.html.twig');
    }
}
