<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Infrastructure\Authentication;

use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberRepositoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class SymfonyMemberProvider
 *
 * @package Orangear\MembershipBundle\Infrastructure\Authentication\Symfony
 */
final class MemberProvider implements UserProviderInterface
{
    /**
     * @var MemberRepositoryInterface
     */
    private $memberRepository;

    /**
     * MemberProvider constructor
     *
     * @param MemberRepositoryInterface $memberRepository
     */
    public function __construct(MemberRepositoryInterface $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        $member = $this->memberRepository->findByEmail($username);

        if ($member instanceof MemberInterface) {
            return $member;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->memberRepository->findByEmail($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class instanceof MemberInterface;
    }
}
