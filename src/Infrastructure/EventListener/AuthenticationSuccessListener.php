<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Infrastructure\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Class AuthenticationSuccessListener
 *
 * @package Orangear\MembershipBundle\Infrastructure\EventListener
 */
class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     *
     * @return void
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof MemberInterface) {
            return;
        }

        $event->setData($data);
    }
}
