<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Infrastructure\Persistence\Repository;

use Orangear\MembershipBundle\Domain\Model\Member\MemberIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberMapperInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberRepositoryInterface;

/**
 * Class MemberRepository
 *
 * @package Orangear\MembershipBundle\Infrastructure\Persistence\Repository
 */
class MemberRepository implements MemberRepositoryInterface
{
    /** @var MemberMapperInterface */
    private $mapper;

    /**
     * MemberRepository constructor
     *
     * @param MemberMapperInterface $mapper
     */
    public function __construct(MemberMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(MemberIdentifierInterface $identifier) :? MemberInterface
    {
        return $this->mapper->fetchById($identifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail(string $email) :? MemberInterface
    {
        return $this->mapper->fetchByEmail($email);
    }
}
