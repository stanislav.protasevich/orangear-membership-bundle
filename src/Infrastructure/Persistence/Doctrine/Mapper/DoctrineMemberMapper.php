<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;
use Orangear\MembershipBundle\Domain\Model\Member\MemberIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberMapperInterface;

/**
 * Class DoctrineMemberMapper
 *
 * @package Orangear\MembershipBundle\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineMemberMapper implements MemberMapperInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * DoctrineMemberMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(MemberIdentifierInterface $identifier) :? MemberInterface
    {
        $repository = $this->entityManager->getRepository(Member::class);

        /** @var MemberInterface $member */
        $member = $repository->findOneBy(['id' => $identifier]);

        return $member;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchByEmail(string $email) :? MemberInterface
    {
        $repository = $this->entityManager->getRepository(Member::class);

        /** @var MemberInterface $member */
        $member = $repository->findOneBy(['email' => $email]);

        return $member;
    }
}
