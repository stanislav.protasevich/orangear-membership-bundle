<?php

declare(strict_types = 1);

namespace Orangear\MembershipBundle\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;
use Orangear\MembershipBundle\Domain\Model\Member\MemberIdentifierInterface;

/**
 * Class MemberIdType
 *
 * @package Orangear\BusinessIntelligencePanel\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class MemberIdType extends GuidType
{
    const MEMBER_ID = 'member_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return MemberId::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var MemberIdentifierInterface $value */

        return $value->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::MEMBER_ID;
    }
}
